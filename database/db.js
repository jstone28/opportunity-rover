const { Client } = require('pg')
const connString = 'postgres://postgres:postgres@postgres:5432/opportunity';
const client = new Client({ connectionString: connString });

const db = {

  createTable: () => {
    client
    .query('CREATE TABLE IF NOT EXISTS opportunity(username VARCHAR(30));')
    .then(res => console.log(res.rows[0]))
    .catch(e => console.error(e.stack))
  },

  connect: () => {
    client.connect(err => {
      if (err) {
        console.error('connection error', err.stack)
      } else {
        console.log('connected')
        module.exports.createTable();
      }
    });
  },

  insert: () => {
    const prefixName = "user-" + Math.random().toString();
    const text = 'INSERT INTO opportunity(username) VALUES($1) RETURNING *'
    const values = [`${prefixName}`]
    console.log(values);
    client
    .query(text, values)
    .then(res => {
      console.log(res.rows[0])
    })
    .catch(e => console.error(e.stack))
  },

  update: () => {
    console.log('update');
  },

  delete: () => {
    console.log('delete')
  },
}

module.exports = db;
